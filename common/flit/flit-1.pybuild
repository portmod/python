# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import glob
import os

try:
    from pybuild import Pybuild2
except ImportError:
    # Fall back if run on portmod <2.4
    from pybuild import Pybuild1 as Pybuild2

from common.wheel import Wheel


class Package(Pybuild2):
    NAME = "Flit"
    DESC = "Pybuild class for installing flit-based python packages"
    KEYWORDS = "openmw fallout-nv oblivion tes3mp fallout-4"


class Flit(Wheel):
    """
    Pybuild class for flit packages

    Note that this assumes the python package is version-independent.
    It just installs a single version into lib/python.
    """

    def __init__(self):
        self.DEPEND += " dev-python/flit_core"

    def src_prepare(self):
        from flit_core.buildapi import build_wheel

        os.mkdir("dist")
        build_wheel("dist")
        for file in glob.glob("dist/*.whl"):
            os.rename(file, file + ".zip")
            print(f"Unpacking wheel {file}")
            self.unpack(file + ".zip")
            self.wheel_path = os.path.join(self.WORKDIR, os.path.basename(file))
        super().src_prepare()

    def src_install(self):
        for pattern in [
            "AUTHORS*",
            "BUGS*",
            "CHANGELOG*",
            "CHANGES*",
            "CREDITS*",
            "FAQ*",
            "NEWS*",
            "README*",
            "THANKS*",
            "TODO*",
        ]:
            self.dodoc(pattern)
        os.chdir(self.wheel_path)
        super().src_install()
