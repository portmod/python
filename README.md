# Python

This is the [Portmod](https://gitlab.com/portmod/portmod) package repository for Python libraries. This repository will usually be pulled in as a dependency and has little purpose on its own.
