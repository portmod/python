# Copyright Copyright 2019-2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import shutil

from pybuild.info import P

from common.distutils import Distutils


class Package(Distutils):
    NAME = "FontTools"
    DESC = " A library to manipulate font files from Python."
    LICENSE = "MIT"
    KEYWORDS = ""
    HOMEPAGE = "https://github.com/fonttools/fonttools"
    # Note: source archive from github includes a symlink
    # which won't extract on windows without administrator permissions
    SRC_URI = "https://files.pythonhosted.org/packages/35/7c/d2f1b8e9e8bd38a5b2334cf88221a39264b0b9a512ff14526bd4d6470c0c/FontTools-2.3.tar.gz#sha256=5397a28a4d1c0420f4f801165f1fdf34a2f41614edfd2e9cf1de264a259bdeaa -> FontTools-2.3.tar.gz"
    S = f"{P}/{P}"
    DEPEND = "dev-python/setuptools"

    def src_install(self):
        super().setup_py("--without-cython")
        # We just want the python library, not the scripts
        shutil.rmtree(os.path.join(self.D, "bin"))
        # The manpages for ttx executable are likewise not useful
        shutil.rmtree(os.path.join(self.D, "share"))
